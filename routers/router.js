const express = require("express");
const router = express.Router();
const userData = require('./login/admin.json')

var isAuth = false;                             

router.post('/', (req,res) => {
    const { username , password} = req.body;
    const user = userData.find(data => {        
        return data.username === username           
    })                                                 

    if (!user){
        isAuth = false;
        // return res.status(404).json({
        //     message: "user non exist"
        // })
        res.status(404).render('games/login');
    } else if (user.password!==password){
        isAuth = false;
        // return res.status(401).json({
        //     message: "wrong pwd"
        // })
        res.status(401).render('games/login');
    } else {

        isAuth = true;
        // return res.status(200).json({
        //     message: "login success"
        // })

        res.status(200).redirect('/dashboard')
    }
})

router.get('/', (req,res) => {
    if(isAuth){
        // return res.status(200).send("login success")
        // return res.status(200).json({
        //     message: "login success"
        // })
        res.status(200).redirect('/dashboard')
    }
    // return res.status(401).send("login failed")
    // return res.status(401).json({
    //     message: "login failed"
    // })
    res.status(401).render('games/login');
})

const auth = () => {
    return isAuth;
}

module.exports = {
    router,
    auth
};

