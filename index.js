const express = require('express')
const { router, auth } = require('./routers/router')
const app = express()
const port = 5000
const { user_game, user_game_biodata, user_game_history } = require('./models')

app.use(express.urlencoded({ extended: false }))

app.set('view engine', 'ejs')

app.use(express.json())
app.use('/login', router)

const loginAuth = (req, res, next) => {
  if (!auth()) {
    res.render('games/login')
  } else{
    next()
  }
}
app.use(loginAuth)

app.get('/dashboard', (req, res) => {
  user_game_history
    .findAll({
      include: [{ model: user_game, as: 'user_game' }],
    })
    .then((games) => res.render('games/index', { games }))
})

app.get('/create', (req, res) => {
  user_game.findAll().then((users) =>
    // x=>console.log(x[0].user_game.username + " is playing for " + x[0].playduration)
    res.render('games/create', { users }),
  )
})

app.post('/insert', async (req, res) => {
  // get user_game record,
  // insert ke user_game_history
  // redirect ke dashboard
  const user_game_record = await user_game.findOne({
    where: { username: req.body.username },
  })

  console.log(user_game_record)

  user_game_history
    .create({
      score: req.body.score,
      playduration: req.body.playDuration,
      user_id: user_game_record.id,
    })
    .then((x) => {
      res.redirect('/dashboard')
    })
})

app.get('/delete/:id', (req, res) => {
  user_game_history
    .destroy({
      where: {
        id: req.params.id,
      },
    })
    .then((x) => res.redirect('/dashboard'))
})

app.use((req, res, next) => {
  res.status(404).sendFile(__dirname + '/errors/404.html')
})

app.listen(port, () => {
  console.log(`Now listening on port ${port}`)
})
